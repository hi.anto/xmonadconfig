-- Imports

-- links:
-- -- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Hooks-Place.html

import XMonad

import XMonad.Config.Desktop
import XMonad.Config.Xfce

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops

import XMonad.Util.EZConfig

import XMonad.Layout.ShowWName
import XMonad.Layout.Fullscreen

import MyVariables
import Var.Theme.AdwaitaAppearance
import MyAutostart
import MyBindings
import MyManageHooks

-- Main function
main = xmonad
    $ ewmh
    $ docks
    $ fullscreenSupport
    $ myConfig

myConfig = def
    {
        workspaces = myWS,
        
        modMask = myModMask,

        handleEventHook = myHandleEvHooks,
        startupHook = ewmhDesktopsStartup <+> myStartupHook,
        manageHook = myManageHooks,
        logHook = ewmhDesktopsLogHook,
        layoutHook = avoidStruts $ showWName' mySWNC myLayout,

        focusFollowsMouse = False,
        clickJustFocuses = True,

        borderWidth = myBorderWidth,
        normalBorderColor = myBGC,
        focusedBorderColor = myAccentC
    } `removeKeysP` myUnBinds `additionalKeysP` myBinds
