-- MyManageHooks.hs

-- links:
-- https://wiki.haskell.org/Xmonad/General_xmonad.hs_config_tips#Making_window_float_by_default.2C_or_send_it_to_specific_workspace

module MyManageHooks where

-- Imports
import XMonad

import XMonad.Hooks.ManageDocks
import XMonad.Layout.Fullscreen

myManageHooks = composeAll
    [
        -- term ws (1)
        className =? "Xfce4-terminal" --> doShift "1",
        
        -- browser ws (2)
        className =? "Firefox" --> doShift "2",
        
        -- code ws (3)
        className =? "Geany" --> doShift "3",
        className =? "Emacs" --> doShift "3",
        className =? "Mysql-workbench-bin" --> doShift "3",
        -- jetbrains ide bootup window
        title =? "win0" --> doFloat,
        title =? "win0" --> doShift "3",
        -- jetbrains ide welcome window
        title =? "Welcome to PyCharm" --> doFloat,
        title =? "Welcome to PyCharm" --> doShift "3",
        title =? "Welcome to IntelliJ IDEA" --> doFloat,
        title =? "Welcome to IntelliJ IDEA" --> doShift "3",
        
        -- system ws (4)
        className =? "Thunar" --> doShift "4",
        
        -- comm ws (5)
        className =? "TelegramDesktop" --> doShift "5",
        
        -- multimedia ws (6)
        className =? "FreeTube" --> doShift "6",
        className =? "Parole" --> doShift "6",
        className =? "vlc" --> doShift "6",
        className =? "mpv" --> doShift "6",
        
        -- cg ws (7)
        className =? "Inkscape" --> doShift "7",
        className =? "Gimp-2.10" --> doShift "7",
        
        className =? "Blender" --> doShift "7",
        title =? "Blender Preferences" --> doFloat,
        title =? "Blender Preferences" --> doShift "7",
        
        className =? "krita" --> doShift "7",
        
        title =? "Change Foreground Color" --> doFloat,
        title =? "Change Foreground Color" --> doShift "7",
        title =? "Change Background Color" --> doFloat,
        title =? "Change Background Color" --> doShift "7",
        title =? "Grow Selection" --> doFloat,
        title =? "Grow Selection" --> doShift "7",
        
        -- random ws (9)
        className =? "Gpick" --> doFloat,
        className =? "Gpick" --> doShift "9",
        
        className =? "Gucharmap" --> doFloat,
        className =? "Gucharmap" --> doShift "9",
        
        className =? "Xfce4-display-settings" --> doFloat,
        className =? "Xfce4-display-settings" --> doShift "9",
        
        className =? "Xfce4-keyboard-settings" --> doFloat,
        className =? "Xfce4-keyboard-settings" --> doShift "9",
        
        className =? "Xfce4-mouse-settings" --> doFloat,
        className =? "Xfce4-mouse-settings" --> doShift "9",
        
        className =? "Xfce4-appearance-settings" --> doFloat,
        className =? "Xfce4-appearance-settings" --> doShift "9",
        
        className =? "Xfce4-settings-manager" --> doFloat,
        className =? "Xfce4-settings-manager" --> doShift "9",
        
        className =? "Xfdesktop-settings" --> doFloat,
        className =? "Xfdesktop-settings" --> doShift "9",
        
        -- float programs
        className =? "Xfce4-about" --> doFloat,
        className =? "Xfce4-appfinder" --> doFloat,
        className =? "Xfce4-clipman-history" --> doFloat,
        className =? "Galculator" --> doFloat,
        className =? "Pavucontrol" --> doFloat,
        className =? "Xfce-polkit" --> doFloat,
        className =? "Xarchiver" --> doFloat,
        title =? "Bluetooth" --> doFloat,
        title =? "Clock" --> doFloat,
        title =? "Action Buttons" --> doFloat,
        title =? "PulseAudio Panel Plugin" --> doFloat,
        title =? "Configuration" --> doFloat,
        title =? "File Operation Progress" --> doFloat,
        title =? "Panel Preferences" --> doFloat,
        title =? "Add New Items" --> doFloat,
        title =? "Separator" --> doFloat,
        title =? "Window Menu" --> doFloat,
        
        -- hooks
        manageDocks,
        fullscreenManageHook
    ]
