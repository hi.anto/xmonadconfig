-- MyVariables.hs

-- links:
-- how i find the xfce4-notifyd binary: https://koji.fedoraproject.org/koji/rpminfo?rpmID=24961274
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-ShowText.html
-- https://github.com/xmonad/xmonad/blob/master/man/xmonad.hs
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Spacing.html#g:1
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Decoration.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Tabbed.html
-- https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-IfMax.html

-- define module name, if we not do this ghc will complain module name
-- does not match file name
-- N: module name and file name should be the same

module MyVariables where

-- imports
import XMonad
import XMonad.Prompt
import XMonad.Actions.WindowBringer

import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ShowWName
import XMonad.Layout.Fullscreen as F
import XMonad.Layout.SubLayouts
import XMonad.Layout.Simplest
import XMonad.Layout.Tabbed
import XMonad.Layout.WindowNavigation
import XMonad.Layout.IfMax
import XMonad.Layout.NoBorders

import XMonad.Hooks.Minimize
import XMonad.Hooks.EwmhDesktops

import Var.MyLayout
import Var.Theme.AdwaitaAppearance
import Var.Theme.AdwaitaTheme

-- workspaces
myWS :: [String]
myWS = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

-- key
myModMask :: KeyMask
myModMask = mod4Mask

-- terminal
myTerm :: String
myTerm = "xfce4-terminal"

-- layout
myLayout = id
    . smartBorders
    . mkToggle (NOBORDERS ?? FULL ?? EOT)
    . mkToggle (single MIRROR)
    $ windowNavigation
    $ addTabsBottom shrinkText myThemeAdwaita
    $ subLayout [] (Simplest)
    $ IfMax 4 tiled (tabbedBottomAlways shrinkText myThemeAdwaita) ||| tabbedBottomAlways shrinkText myThemeAdwaita

-- SWNConfig
mySWNC :: SWNConfig
mySWNC = def
    {
        swn_font = myFontB,
        swn_bgcolor = myBGC,
        swn_color = myFGC,
        swn_fade = 0.8
    }

-- XPConfig
myXPConfig :: XPConfig
myXPConfig = def
    {
        font = myFont,
        bgColor = myBGC,
        fgColor = myFGC,
        bgHLight = myAccentC,
        fgHLight = myFGC,
        borderColor = myAccentC,
        promptBorderWidth = myBorderWidth,
        position = Top,
        alwaysHighlight = True,
        height = 22,
        historySize = 0
    }

-- hooks
myHandleEvHooks = F.fullscreenEventHook
    <+> minimizeEventHook
    <+> ewmhDesktopsEventHook
