-- MyBindings.hs

-- links:
-- http://hackage.haskell.org/package/xmonad-contrib-0.15/docs/XMonad-Util-EZConfig.html
-- https://wiki.haskell.org/Xmonad/General_xmonad.hs_config_tips#Key_and_Mouse_Bindings
-- http://hackage.haskell.org/package/xmonad-contrib-0.15/docs/XMonad-Util-EZConfig.html#v:mkKeymap
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Spacing.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-MultiToggle.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Prompt-ConfirmPrompt.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Prompt-Window.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-WithAll.html

module MyBindings where

-- Imports
import XMonad

import System.Exit

import XMonad.Util.EZConfig

import XMonad.Prompt.Pass (passPrompt)
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Window
import XMonad.Prompt.Shell

import XMonad.Actions.CycleWS
import XMonad.Actions.WithAll
import XMonad.Actions.FloatKeys
import XMonad.Actions.Minimize

import XMonad.Layout.Spacing
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.ResizableTile
import XMonad.Layout.Hidden

import qualified XMonad.StackSet as W

import MyVariables
import Var.Theme.AdwaitaAppearance

myBinds =
    [
        -- Xmonad
        ("M-<Esc>", spawn "xmonad --recompile; xmonad --restart"),
        ("M-M1-<Esc>", spawn "xfce4-session-logout"),
        
        -- shoot
        ("<Print>", spawn "flameshot full -c -p ~/screenshots/ -d 1000"),
        ("M-<Print>", spawn "flameshot gui -p ~/screenshots/ -d 1000"),
        
        ("M1-<Print> b", spawn "$HOME/.local/mybin/makeup-ss-bg.sh b"),
        ("M1-<Print> g", spawn "$HOME/.local/mybin/makeup-ss-bg.sh g"),
        ("M1-<Print> C-b", spawn "$HOME/.local/mybin/makeup-ss-bg.sh br"),
        ("M1-<Print> S-C-b", spawn "$HOME/.local/mybin/makeup-ss-bg.sh bbr"),
        ("M1-<Print> C-g", spawn "$HOME/.local/mybin/makeup-ss-bg.sh gr"),
        ("M1-<Print> S-C-g", spawn "$HOME/.local/mybin/makeup-ss-bg.sh gbr"),
        
        ("M1-<Print> M1-b", spawn "$HOME/.local/mybin/makeup-ss-author.sh b"),
        ("M1-<Print> M1-g", spawn "$HOME/.local/mybin/makeup-ss-author.sh g"),
        ("M1-<Print> M1-C-b", spawn "$HOME/.local/mybin/makeup-ss-author.sh br"),
        ("M1-<Print> M1-S-C-b", spawn "$HOME/.local/mybin/makeup-ss-author.sh bbr"),
        ("M1-<Print> M1-C-g", spawn "$HOME/.local/mybin/makeup-ss-author.sh gr"),
        ("M1-<Print> M1-S-C-g", spawn "$HOME/.local/mybin/makeup-ss-author.sh gbr"),
        
        ("M1-<Print> r", spawn "$HOME/.local/mybin/makeup-ss-transparent.sh r"),
        ("M1-<Print> S-r", spawn "$HOME/.local/mybin/makeup-ss-transparent.sh br"),
        ("M1-<Print> s", spawn "$HOME/.local/mybin/makeup-ss-transparent.sh s"),
        ("M1-<Print> C-s", spawn "$HOME/.local/mybin/makeup-ss-transparent.sh sr"),
        ("M1-<Print> S-C-s", spawn "$HOME/.local/mybin/makeup-ss-transparent.sh sr"),
        
        -- info
        ("M-i d", spawn "$HOME/.local/mybin/show-date.sh"),
        ("M-i t", spawn "$HOME/.local/mybin/show-time.sh"),
        ("M-i b", spawn "$HOME/.local/mybin/show-bat.sh"),
        
        -- display
        ("M-d c", spawn "$HOME/.local/mybin/toggle-compton.sh"),
        
        -- input
        ("M1-<Insert>", spawn "$HOME/.local/mybin/toggle-touchpad.sh"),
        
        -- volumme + brightness
        ("<XF86MonBrightnessUp>", spawn "$HOME/.local/mybin/monbr-up.sh"),
        ("<XF86MonBrightnessDown>", spawn "$HOME/.local/mybin/monbr-down.sh"),
        ("M-M1-]", spawn "$HOME/.local/mybin/monbr-up.sh"),
        ("M-M1-[", spawn "$HOME/.local/mybin/monbr-down.sh"),
        
        ("<XF86AudioRaiseVolume>", spawn "amixer -qD pipewire -- sset Master 1%+"),
        ("<XF86AudioLowerVolume>", spawn "amixer -qD pipewire -- sset Master 1%-"),
        ("<XF86AudioMute>", spawn "amixer -qD pipewire -- sset Master toggle"),
        
        -- media player
        ("M-M1-<Space>", spawn "playerctl play-pause"),
        ("M-M1-.", spawn "playerctl next"),
        ("M-M1-,", spawn "playerctl previous"),
        ("M-M1-s", spawn "playerctl stop"),
        
        -- prompt / launcher
        ("M-p r", shellPrompt myXPConfig),
        ("M-p d", spawn "xfce4-appfinder"),
        ("M-p p", passPrompt myXPConfig),
        ("M-p w", spawn "$HOME/.local/mybin/wallpaper-mager.sh"),
        
        -- gaps / border
        ("M-'", toggleSmartSpacing),
        ("M-S-'", toggleWindowSpacingEnabled),
        ("M-C-'", incWindowSpacing 2),
        ("M-M1-'", decWindowSpacing 2),
        ("M-;", setWindowSpacing myWBorder),
        ("M-b", sendMessage $ Toggle NOBORDERS),
        
        -- workspaces and screens
        ("M-]", nextWS),
        ("M-[", prevWS),
        ("M-S-]", shiftToNext >> nextWS),
        ("M-S-[", shiftToPrev >> prevWS),
        ("M-.", nextScreen),
        ("M-,", prevScreen),
        ("M-S-.", shiftNextScreen),
        ("M-S-,", shiftPrevScreen),
        ("M-`", toggleWS),
        
        -- layout
        ("M-M1-m", sendMessage $ Toggle MIRROR),
        ("M-f", sendMessage $ Toggle FULL),
        
        -- window
        ("M-w g", windowPrompt myXPConfig Goto allWindows),
        ("M-w b", windowPrompt myXPConfig Bring allWindows),
        -- hide unHide
        ("M-q", withFocused minimizeWindow),
        ("M-S-q", withLastMinimized maximizeWindowAndFocus),
        -- (float)
        ("M-<Right>", withFocused (keysMoveWindow (2, 0))), -- move window 2 pix right
        ("M-<Down>", withFocused (keysMoveWindow (0, 2))), -- move window 2 pix down
        ("M-<Up>", withFocused (keysMoveWindow (0, -2))), -- move window 2 pix up
        ("M-<Left>", withFocused (keysMoveWindow (-2, 0))), -- move window 2 pix left
        
        ("M-S-<Right>", withFocused (keysResizeWindow (4, 0) (0, 0))), -- resize window 4 pix right
        ("M-S-<Down>", withFocused (keysResizeWindow (0, 4) (0, 0))), -- resize window 4 pix down
        ("M-S-<Up>", withFocused (keysResizeWindow (0, -4) (0, 0))), -- resize window 4 pix up
        ("M-S-<Left>", withFocused (keysResizeWindow (-4, 0) (0, 0))), -- resize window 4 pix left
        
        ("M-/", withFocused (keysMoveWindowTo (683,384) (0.5, 0.5))),
        ("M-t", withFocused (windows . W.sink)),
        ("M-S-t", sinkAll),
        -- (tile)
        ("M-S-<Return>", windows W.swapMaster),
        
        -- layout
        ("M-<Space>", sendMessage NextLayout),
        -- (sublayout)
        ("M-C-h", sendMessage $ pullGroup L),
        ("M-C-j", sendMessage $ pullGroup D),
        ("M-C-k", sendMessage $ pullGroup U),
        ("M-C-l", sendMessage $ pullGroup R),
        
        ("M-m .", withFocused (sendMessage . MergeAll)),
        ("M-m ;", withFocused (sendMessage . UnMerge)),
        
        ("M-C-]", onGroup W.focusUp'),
        ("M-C-[", onGroup W.focusDown'),
        -- (rTall)
        ("M-h", sendMessage Shrink),
        ("M-l", sendMessage Expand),
        ("M-M1-j", sendMessage MirrorShrink),
        ("M-M1-k", sendMessage MirrorExpand),
        
        ("M-x", kill),
        ("M-<Return>", spawn myTerm)
    ]
    
myUnBinds =
    [
        -- unbind default for restart and quit xmonad
        ("M-q"), ("M-S-q"),
        
        -- unbind default for launch dmenu
        ("M-p"),
        
        -- unbind default for closing window
        ("M-S-c"),
        
        -- unbind default for launch terminal
        ("M-S-<Return>"),
        
        -- unbind default for sink window to tiling
        ("M-t"),
        
        -- unbind default for shirk and expand master size in Tile layout
        ("M-h"),
        ("M-L"),
        
        -- unbind default for pyshical screen
        ("M-w"), ("M-S-w"),
        ("M,e"), ("M-S-e"),
        ("M-r"), ("M-S-r"),
        
        -- unbind default for push window back to tiling
        ("M-t"),
        
        -- unbind default for help
        ("M-S-/")
    ]
