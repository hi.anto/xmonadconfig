-- MyLayout.hs

-- links:
-- https://gitlab.com/dwt1/dotfiles/-/tree/master/.xmonad#layouts

-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Minimize.html
-- or
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-Hidden.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Hooks-Minimize.html
-- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-Minimize.html

module Var.MyLayout where

-- imports
import XMonad

import XMonad.Layout.ResizableTile
import XMonad.Layout.Minimize

import Var.Theme.AdwaitaTheme
import Var.Theme.AdwaitaAppearance

tiled = minimize (ResizableTall nMaster delta ratio slave)
    where
        nMaster = 1 -- number of master windows
        delta = 2/100 -- change when resizing by Shrink, Expand, MirrorShrink, MirrorExpand
        ratio = 1.2/2 -- width of master
        -- fraction to multiply the window height that would be given when divided equally.
        -- slave windows are assigned their modified heights in order, from top to bottom
        -- unspecified values are replaced by 1
        slave = []
