-- SnsrTheme.hs

module Var.Theme.SnsrTheme where

-- import
import XMonad.Layout.Decoration

import Var.Theme.SnsrAppearance

myThemeSnsr :: Theme
myThemeSnsr = def
    {
        activeColor = myAccentC,
        inactiveColor = myBGC,
        urgentColor = myUrgC,
        
        activeBorderColor = myAccentC,
        inactiveBorderColor = myBGC,
        urgentBorderColor = myUrgC,
        
        activeBorderWidth = myBorderWidth,
        inactiveBorderWidth = myBorderWidth,
        urgentBorderWidth = myBorderWidth,
        
        activeTextColor = myFGC,
        inactiveTextColor = myLightGreyC,
        urgentTextColor = myFGC,
        
        fontName = myFont
    }
