-- SnsrAppearance.hs

module Var.Theme.SnsrAppearance where

-- imports
import XMonad

import XMonad.Layout.Spacing

myFont :: String
myFont = "xft:RobotoMono Nerd Font:regular:size=10:antialias=true:hinting=true"
myFontB :: String
myFontB = "xft:RobotoMono Nerd Font:regular:size=32:antialias=true:hinting=true"

myBorderWidth :: Dimension
myBorderWidth = 3

myWBorder :: Border
myWBorder = (Border 0 3 3 0)
mySBorder :: Border
mySBorder = (Border 3 0 0 3)

-- color
myFGC :: String
myFGC = "#f2f2f2"

myGreyC :: String
myGreyC = "#2E2923"

myLightGreyC :: String
myLightGreyC = "#89847e"

myBGC :: String
myBGC = myGreyC

myAccentC :: String
myAccentC = "#af3908"

myLightAccentC :: String
myLightAccentC = "#fc6626"

myDarkAccentC :: String
myDarkAccentC = "#632002"

myAccentC2 :: String
myAccentC2 = "#006356"

myLightAccentC2 :: String
myLightAccentC2 = "#09B099"

myUrgC :: String
myUrgC = "#f34a10"
