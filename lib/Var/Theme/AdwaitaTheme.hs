-- AdwaitaTheme.hs

module Var.Theme.AdwaitaTheme where

-- import
import XMonad.Layout.Decoration

import Var.Theme.AdwaitaAppearance

myThemeAdwaita :: Theme
myThemeAdwaita = def
    {
        activeColor = myAccentC,
        inactiveColor = myBGC,
        urgentColor = myUrgC,
        
        activeBorderColor = myAccentC,
        inactiveBorderColor = myBGC,
        urgentBorderColor = myUrgC,
        
        activeBorderWidth = myBorderWidth,
        inactiveBorderWidth = myBorderWidth,
        urgentBorderWidth = myBorderWidth,
        
        activeTextColor = myLightGreyC,
        inactiveTextColor = myFGC,
        urgentTextColor = myFGC,
        
        fontName = myFont
    }
